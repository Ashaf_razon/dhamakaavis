package com.avis.avistechbd.dhamakaavis.view_side;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.dhamakaavis.Activities.AudioPlay;
import com.avis.avistechbd.dhamakaavis.Activities.VideoPreview;
import com.avis.avistechbd.dhamakaavis.R;
import com.avis.avistechbd.dhamakaavis.network_retrieve_common.CommonListData;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;


public class ContentList extends RecyclerView.Adapter<ContentList.ViewHolder>{
    //var
    private final String TAG = "ListOfContent";
    CommonListData commonListDataSend;
    private Context context;

    public ContentList(@Nullable CommonListData commonListData, Context context) {
        this.commonListDataSend = commonListData;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_content_masti,viewGroup,false);
        ContentList.ViewHolder holder = new ContentList.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        Log.d(TAG,"Bind View Holder Called");
        try{
            viewHolder.contentNameSt.setText("Title: "+commonListDataSend.contTitle.get(i));
            viewHolder.contentViewSt.setText(commonListDataSend.contDuration.get(i));
            viewHolder.contDescrSt.setText("Info: "+commonListDataSend.contInfo.get(i)+"\nAlbum: "+commonListDataSend.contAlbum.get(i));
            Glide.with(this.context)
                    .load(commonListDataSend.contImageURL.get(i))
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.contentImageSt);
        }catch (Exception e){
            Log.d("bindView","error: "+e.getMessage());
        }
        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(commonListDataSend.getContVideoURL().get(i).contains(".mp3")){
                    Intent intent = new Intent(context, AudioPlay.class);
                    try{
                        intent.putExtra("audioLen",commonListDataSend.getContLength().get(i));
                        intent.putExtra("audioID",commonListDataSend.getContVideoID().get(i));
                        intent.putExtra("audioURL",commonListDataSend.getContVideoURL().get(i));
                        intent.putExtra("audioImageURL",commonListDataSend.getContImageURL().get(i));
                    }catch (Exception e){
                        Log.d("ContentList Log",e.getMessage()+" "+commonListDataSend.getContVideoURL().get(i));
                    }
                    (context).startActivity(intent);
                }else{
                    Intent intent = new Intent(context, VideoPreview.class);
                    intent.putExtra("videoURL",commonListDataSend.getContVideoURL().get(i));
                    (context).startActivity(intent);
                }
                //Toast.makeText(v.getContext(),commonListDataSend.getContVideoID().get(i)+" <> "+commonListDataSend.getContVideoURL().get(i),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return commonListDataSend.contDuration.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView contentNameSt,contentViewSt, contDescrSt;
        ImageView contentImageSt;
        RelativeLayout parentLayout;
        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            try{
                contentNameSt = itemView.findViewById(R.id.textView12);
                contentImageSt = itemView.findViewById(R.id.imageView3);
                contentViewSt = itemView.findViewById(R.id.textView13);
                contDescrSt = itemView.findViewById(R.id.textView);
            }catch (Exception e){
                Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
                Log.d("TagContentList",""+e.getMessage());
            }
            parentLayout = itemView.findViewById(R.id.relative_Layout_Content_list);
        }
    }
}
