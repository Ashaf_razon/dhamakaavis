package com.avis.avistechbd.dhamakaavis.Activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import com.avis.avistechbd.dhamakaavis.R;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;

import java.io.IOException;

import butterknife.ButterKnife;

public class VideoPreview extends AppCompatActivity implements
        SurfaceHolder.Callback,
        MediaPlayer.OnPreparedListener,
        SeekBar.OnSeekBarChangeListener,
        MediaPlayer.OnBufferingUpdateListener,
        MediaPlayer.OnCompletionListener{
    //local variables
    private static final double MAX_CONTENT_DURATION = 1281.0;
    private String videoURL = "http://cms.bdscreens.com/FullVideo/D1/Shopno%20Bheja%20Megh%20-%20MINAR%20-%20PUJA%20-%20First%20Love%20-%20Apurba%20-%20Mehazabien%20-%20Antu%20-%20New%20Song%202019.mp4";
    private ImageView videoViewImages[] = new ImageView[8];
    private TextView videoViewText[] = new TextView[3];
    private SeekBar videoViewSeekBar;
    //app variables
    private Context context;
    //video variables
    private ProgressBar spinKitBar;
    //private ProgressDialog pd;
    private Handler mediaPlayerHandler;
    private LinearLayout layoutHeader,layoutBottom,layoutMiddle;
    boolean headTailsVisible = true;
    boolean controller1ControlsVisibility = false;
    boolean screenOrientation = true; //default portrait
    private SurfaceView surfaceView;
    private ImageView backPressed, playVideo;
    private TextView videoName;
    private MediaPlayer mediaPlayer;
    private SurfaceHolder surfaceHolder;
    Handler videoHandler;
    Runnable videoRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_details);

        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        videoURL = bundle.getString("videoURL");
    }
    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_details);
        showMsg("onStart");
        try{
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getSupportActionBar().hide();
        }catch (Exception e){
            e.printStackTrace();
        }
        initUIvariables();
        setHandler();
        initializeSeekBar();
    }

    private void setHandler() {
        videoHandler = new Handler();
        videoRunnable = new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer.getDuration() > 0) {
                    int currentVideoDuration = mediaPlayer.getCurrentPosition();
                    videoViewSeekBar.setProgress(currentVideoDuration);
                    videoViewText[1].setText("" + convertIntoTime(currentVideoDuration));
                    double getMediaDur = mediaPlayer.getDuration() / 1000;
                    videoViewText[2].setText("-" + convertIntoTime(mediaPlayer.getDuration() - currentVideoDuration));
                }

                videoHandler.postDelayed(this, 1000);
            }
        };
        /*
         * To Start the handler
         * */
        videoHandler.postDelayed(videoRunnable, 5000);
    }
    /*
     * This function can convert the time from int milliSecond/ long milliSecond to String format  like 12:00, 23:00
     * */

    private String convertIntoTime(int ms) {
        String time;
        int x, seconds, minutes, hours;
        x = (int) (ms / 1000);
        seconds = x % 60;
        x /= 60;
        minutes = x % 60;
        x /= 60;
        hours = x % 24;
        if (hours != 0)
            time = String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
        else time = String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
        return time;
    }


    private void initUIvariables() {
        context = VideoPreview.this;
        //layout
        layoutHeader = findViewById(R.id.videoViewHeaderLayout);
        layoutBottom = findViewById(R.id.videoViewSeekbarTimeLayout);
        layoutMiddle = findViewById(R.id.videoViewController1);
        //images
        videoViewImages[0] = findViewById(R.id.videoViewBackPress);
        videoViewImages[1] = findViewById(R.id.videoViewContentImage);
        videoViewImages[2] = findViewById(R.id.videoViewCast);
        videoViewImages[3] = findViewById(R.id.videoViewRewindVideo);
        videoViewImages[4] = findViewById(R.id.videoViewPauseVideo);
        videoViewImages[5] = findViewById(R.id.videoViewForwardVideo);
        videoViewImages[6] = findViewById(R.id.videoViewVolume);
        videoViewImages[7] = findViewById(R.id.videoViewFullScreen);
        //text
        videoViewText[0] = findViewById(R.id.videoViewContentTitle);
        videoViewText[1] = findViewById(R.id.videoViewTxtCurrentTime);
        videoViewText[2] = findViewById(R.id.videoViewTxtTotalDuration);
        //seek
        videoViewSeekBar = findViewById(R.id.videoViewSeekbar);
        //video UI init
        spinKitBar = (ProgressBar)findViewById(R.id.spin_kit);
        Sprite doubleBounce = new DoubleBounce();
        spinKitBar.setIndeterminateDrawable(doubleBounce);
        mediaPlayerHandler = new Handler(Looper.getMainLooper());
        mediaPlayer = new MediaPlayer();
        surfaceView = findViewById(R.id.videoViewSurfaceView);
        surfaceHolder = surfaceView.getHolder();
        surfaceView.setKeepScreenOn(true);
        surfaceHolder.addCallback(this);
    }

    private void initializeSeekBar() {
        videoViewSeekBar.setProgress(0);
        videoViewSeekBar.setOnSeekBarChangeListener(this);
    }


    public void videoViewBackPressClick(View view) {
        showMsg("Back");
        onBackPressed();
    }

    public void videoViewCastClick(View view) {
        showMsg("Cast");
    }

    public void videoViewSurfaceViewClick(View view) {
        showMsg("Surface");
        layoutsConfigChangeTopBottom();
    }

    public void videoViewController1Click(View view) {
        showMsg("Controller 1");
        layoutsConfigChangeTopBottom();
    }

    public void videoViewRewindVideoClick(View view) {
        showMsg("Rewind");
        if(mediaPlayer.getCurrentPosition()-30000 >= 0.0)
            mediaPlayer.seekTo(mediaPlayer.getCurrentPosition()-30000);
    }

    public void videoViewPauseVideoClick(View view) {
        showMsg("Pause OR Play");
        try{
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
                videoViewImages[4].setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
            } else {
                mediaPlayer.start();
                videoViewImages[4].setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
            }
        }catch (Exception e){
            showMsg(e.getMessage().toString());
        }
    }

    public void videoViewForwardVideo(View view) {
        showMsg("Forward ");
        if((mediaPlayer.getCurrentPosition()+40000) <= mediaPlayer.getDuration())
            mediaPlayer.seekTo(mediaPlayer.getCurrentPosition()+30000);
    }

    public void videoViewVolumeClick(View view) {
        showMsg("Volume");
    }

    public void videoViewFullScreenClick(View view) {
        showMsg("FullScreen");
        setOrientationResponse();
    }

    void showMsg(String msg){
        //Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
        Log.d("MustPlay"," : "+msg);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mediaPlayer.start();
        showMsg("Prepared");
//        pd.dismiss();
        spinKitBar.setVisibility(View.GONE);
        /*
         * Now to set the max Value of SeekBar
         * */
        videoViewSeekBar.setMax(mediaPlayer.getDuration());
    }

    @Override
    public void surfaceCreated(final SurfaceHolder holder) {
        mediaPlayer.setDisplay(holder);
        try{
            mediaPlayer.setDataSource(videoURL);
            mediaPlayer.prepareAsync();
        }catch (IOException E){
            E.printStackTrace();
        }
        mediaPlayer.setOnPreparedListener(VideoPreview.this);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()){
            case R.id.videoViewSeekbar:
                if (fromUser) {//only if user manually seeks the seekBar....Important
                    mediaPlayer.seekTo(progress);
                    int currentVideoDuration = mediaPlayer.getCurrentPosition();
                    videoViewText[1].setText("" + convertIntoTime(currentVideoDuration));
                    videoViewText[2].setText("-" + convertIntoTime(mediaPlayer.getDuration() - currentVideoDuration));
                }
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        try{
            releaseMediaPlayer();
            onStart();
        }catch (Exception e){
            showMsg("Config Change "+e.getMessage());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        showMsg("onStop");
        try{
            mediaPlayer.pause();
            videoViewImages[4].setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
            onSurfaceTuchAndroidMenueShow();
        }catch (Exception e){
            showMsg("onPause : "+e.getMessage());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseMediaPlayer();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        releaseMediaPlayer();
    }
    private void releaseMediaPlayer() {
        if (mediaPlayer != null) {
            /*
             * Remove Callback from the handler...Important
             * */
            try{
                videoHandler.removeCallbacks(videoRunnable);
                mediaPlayer.release();
                mediaPlayer = null;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    void layoutsConfigChangeTopBottom(){
        try{
            if(headTailsVisible){
                layoutHeader.setVisibility(View.GONE);
                layoutBottom.setVisibility(View.GONE);
                layoutMiddle.setVisibility(View.GONE);
                onSurfaceTuchAndroidMenueHidden();
                headTailsVisible = false;
            }else{
                layoutHeader.setVisibility(View.VISIBLE);
                layoutBottom.setVisibility(View.VISIBLE);
                layoutMiddle.setVisibility(View.VISIBLE);
                onSurfaceTuchAndroidMenueShow();
                headTailsVisible = true;
            }
        }catch (Exception e){
            showMsg(e.getMessage());
        }
        showMsg(headTailsVisible+" Control 1");
    }

    private void setOrientationResponse() {
        int orientation = VideoPreview.this.getResources().getConfiguration().orientation;
        try{
            if(orientation == Configuration.ORIENTATION_PORTRAIT){
                try{
                    setRequestedOrientation(Build.VERSION.SDK_INT < 9 ?
                            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE :
                            ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }else{
                try{
                    setRequestedOrientation(Build.VERSION.SDK_INT < 9 ?
                            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT :
                            ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            showMsg("On create "+e.getMessage());
        }
    }
    void onSurfaceTuchAndroidMenueHidden(){
        View decorView = getWindow().getDecorView();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);
    }
    void onSurfaceTuchAndroidMenueShow(){
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        videoViewSeekBar.setSecondaryProgress(percent);
        showMsg("Buffering");
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        showMsg("Complete Media");
    }

}
