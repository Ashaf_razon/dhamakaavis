package com.avis.avistechbd.dhamakaavis.Activities.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;
import android.widget.Toast;
import com.avis.avistechbd.dhamakaavis.Activities.MainActivity;
import com.avis.avistechbd.dhamakaavis.R;
import com.avis.avistechbd.dhamakaavis.network_retrieve_common.CommonListData;
import com.avis.avistechbd.dhamakaavis.network_retrieve_common.CommonViewModel;
import com.avis.avistechbd.dhamakaavis.view_side.ContentList;

import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.Unbinder;
public class ListOfContent extends Fragment {
    //user var
    final static int MUSIC_VIDEO = 2;
    final static int AUDIO_ZONE = 3;
    final static int DRAMA_ZONE = 4;
    final static int CINEMA_ZONE = 5;
    private int FLAG = 0;
    //Fragment Var
    private Unbinder unbinder;
    private Context context;

    private RecyclerView recyclerView;
    //var
    private final String TAG = "ListOfContent";

    //Network call
    CommonViewModel commonViewModel;
    Observer<CommonListData> trackCommonDataList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_content_list, container, false);
        context = getActivity();
        MainActivity mainAct = (MainActivity)getActivity();
        unbinder = ButterKnife.bind(this, view);

        try{
            TextView tv_toolbar = ((FragmentActivity) context).findViewById(R.id.toolbar_title);
            switch(Objects.requireNonNull(mainAct).getSetFragmentSelectionFlag()){
                case MUSIC_VIDEO: tv_toolbar.setText("Music"); FLAG = MUSIC_VIDEO; break;
                case AUDIO_ZONE: tv_toolbar.setText("Audio");FLAG = AUDIO_ZONE; break;
                case DRAMA_ZONE: tv_toolbar.setText("Drama");FLAG = DRAMA_ZONE; break;
                case CINEMA_ZONE: tv_toolbar.setText("Cinema");FLAG = CINEMA_ZONE; break;
                default:
                    break;
            }
            //showMsg("Content Category: "+FLAG);
        }catch (Exception e){
            //showMsg("Content: "+e.getMessage());
        }
        commonViewModel = ViewModelProviders.of(getActivity()).get(CommonViewModel.class);
        trackCommonDataList = new Observer<CommonListData>(){
            @Override
            public void onChanged(@Nullable CommonListData commonListData) {
                initRecyclerView(view, commonListData);
            }
        };
        commonViewModel.getCommontListDataConfig(FLAG-1).observe(this,trackCommonDataList);

        return view;
    }

    private void initRecyclerView(View view, @Nullable CommonListData commonListData){
        try{
            recyclerView = view.findViewById(R.id.recycler_view_content_list);
            ContentList adapter = new ContentList(commonListData,context);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            try{
                LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
                recyclerView.setLayoutAnimation(animation);
                Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                recyclerView.scheduleLayoutAnimation();
            }catch (NullPointerException e){
                Log.d("ListContent",""+e.getMessage());
            }
        }catch (Exception e){
            showMsg("recyclerView Error "+e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void showMsg(String msg){
        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
    }
}
