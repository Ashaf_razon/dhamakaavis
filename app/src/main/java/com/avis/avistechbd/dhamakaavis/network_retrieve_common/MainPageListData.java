package com.avis.avistechbd.dhamakaavis.network_retrieve_common;

import java.util.ArrayList;

public class MainPageListData {
    ArrayList<ArrayList<String>> contImageURL = new ArrayList<>();
    ArrayList<ArrayList<String>> contAlbum = new ArrayList<>();
    ArrayList<ArrayList<String>> contTitle = new ArrayList<>();
    ArrayList<ArrayList<String>> contInfo = new ArrayList<>();
    ArrayList<ArrayList<String>> contDuration = new ArrayList<>();
    ArrayList<ArrayList<String>> contLen = new ArrayList<>();
    ArrayList<ArrayList<String>> contVideoURL = new ArrayList<>();
    ArrayList<ArrayList<String>> contVideoID = new ArrayList<>();


    public ArrayList<ArrayList<String>> getContLen() {
        return contLen;
    }

    public void setContLen(ArrayList<ArrayList<String>> contLen) {
        this.contLen = contLen;
    }

    public ArrayList<ArrayList<String>> getContImageURL() {
        return contImageURL;
    }

    public void setContImageURL(ArrayList<ArrayList<String>> contImageURL) {
        this.contImageURL = contImageURL;
    }

    public ArrayList<ArrayList<String>> getContAlbum() {
        return contAlbum;
    }

    public void setContAlbum(ArrayList<ArrayList<String>> contAlbum) {
        this.contAlbum = contAlbum;
    }

    public ArrayList<ArrayList<String>> getContTitle() {
        return contTitle;
    }

    public void setContTitle(ArrayList<ArrayList<String>> contTitle) {
        this.contTitle = contTitle;
    }

    public ArrayList<ArrayList<String>> getContInfo() {
        return contInfo;
    }

    public void setContInfo(ArrayList<ArrayList<String>> contInfo) {
        this.contInfo = contInfo;
    }

    public ArrayList<ArrayList<String>> getContDuration() {
        return contDuration;
    }

    public void setContDuration(ArrayList<ArrayList<String>> contDuration) {
        this.contDuration = contDuration;
    }

    public ArrayList<ArrayList<String>> getContVideoURL() {
        return contVideoURL;
    }

    public void setContVideoURL(ArrayList<ArrayList<String>> contVideoURL) {
        this.contVideoURL = contVideoURL;
    }

    public ArrayList<ArrayList<String>> getContVideoID() {
        return contVideoID;
    }

    public void setContVideoID(ArrayList<ArrayList<String>> contVideoID) {
        this.contVideoID = contVideoID;
    }
}
