package com.avis.avistechbd.dhamakaavis.view_side;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.dhamakaavis.R;

import org.w3c.dom.Text;

import me.biubiubiu.justifytext.library.JustifyTextView;

public class QuestionAnsDialog extends Dialog implements View.OnClickListener {
    //setStatusFire
    private Context context;
    public Activity activity;
    public Button yes;
    TextView txtDia;
    String dialog_title;
    int flagNo;

    public QuestionAnsDialog(Activity activity, String dialog_title, int flagNo, Context context) {
        super(activity);
        this.activity = activity;
        this.dialog_title = dialog_title;
        this.flagNo = flagNo;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_question);
        try{
            yes = (Button) findViewById(R.id.button2);
            txtDia = findViewById(R.id.textView7);
            txtDia.setText(dialog_title);
        }catch (Exception e){
            showStatus(e.getMessage());
        }
        yes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
    private void showStatus(String msg){
        Toast.makeText(context,"FAQ ans: "+msg,Toast.LENGTH_LONG).show();
    }
}
