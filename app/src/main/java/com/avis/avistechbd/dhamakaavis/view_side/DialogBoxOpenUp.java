package com.avis.avistechbd.dhamakaavis.view_side;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.avis.avistechbd.dhamakaavis.R;

public class DialogBoxOpenUp {
    Context context;
    public static int flag = 0,flag2 = 0;
    int localFlag = 0;
    String dialogWarning = null;
    public DialogBoxOpenUp(Context context) {
        this.context = context;
    }

    public void showArchiveDialog() {
        @SuppressLint("ResourceAsColor") final MaterialDialog.Builder builder = new MaterialDialog.Builder(context).
                icon(((Activity)context).getResources().getDrawable(R.drawable.openbook)).
                dividerColor(((Activity)context).getResources().getColor(R.color.textBackColor))
                .title("Content").titleColor(context.getResources().
                        getColor(R.color.black_color)).
                        items(R.array.dialogList).itemsColor(context.getResources().
                        getColor(R.color.contentItemColo))
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        //Toast.makeText(MainActivity.this, which + ": " + text + ", ID = " + view.getId(), Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }).positiveText("OK").onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //Do your Job
                    }
                }).negativeText("Cancel").onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).cancelable(false).dividerColor(context.getResources().getColor(R.color.commBackColor));
        final MaterialDialog dialog = builder.build();
        dialog.show();
    }


    public void createDialogBoxCancellation(String msg,final Menu getNavMenu) {
        this.dialogWarning = msg;
        final MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title("Alert").titleColor(context.getResources().getColor(R.color.black_color)).content(dialogWarning)
                .contentColor(context.getResources().
                        getColor(R.color.dialogDescText)).positiveText("Yes").negativeText("No").
                        cancelable(false).onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        if(dialogWarning.compareToIgnoreCase("Do you want to Exit?") == 0){
                            ((Activity)context).finish();
                        }else if (dialogWarning.compareToIgnoreCase("Do you want to UNSUBSCRIBE ?") == 0){
                            // changeTextSubs2();
                            getNavMenu.findItem(R.id.nav_subscribe).setTitle("SUBSCRIBE");
                            setTextColorForMenuItem(getNavMenu.findItem(R.id.nav_subscribe), R.color.cardA);
                        }
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        setTextColorForMenuItem(getNavMenu.findItem(R.id.nav_subscribe), R.color.cardB);
                        dialog.dismiss();
                    }
                }).dividerColor(context.getResources().getColor(R.color.commBackColor));

        final MaterialDialog dialog = builder.build();
        dialog.show();
    }

    public void showSubsDialog(final Menu getNavMenu) {
        @SuppressLint("ResourceAsColor") final MaterialDialog.Builder builder = new MaterialDialog.Builder(context).
                icon(((Activity)context).getResources().getDrawable(R.drawable.pack)).
                dividerColor(context.getResources().getColor(R.color.textBackColor))
                .title("Select Package").titleColor(context.getResources().
                        getColor(R.color.black_color)).
                        items(R.array.subsList).itemsColor(context.getResources().
                        getColor(R.color.dialogDescText))
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        localFlag = which;
                        Toast.makeText(context, "Clicked Item : "+localFlag, Toast.LENGTH_SHORT).show();
                        if(localFlag >= 0){
                            //changeTextSubs();
                            //Toast.makeText(context, "Clicked Item : "+localFlag, Toast.LENGTH_SHORT).show();
                            getNavMenu.findItem(R.id.nav_subscribe).setTitle("UNSUBSCRIBE");
                            setTextColorForMenuItem(getNavMenu.findItem(R.id.nav_subscribe), R.color.contentItemColo);
                        }
                        return true;
                    }
                }).positiveText("Select One").negativeText("Cancel").onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        setTextColorForMenuItem(getNavMenu.findItem(R.id.nav_subscribe), R.color.cardA);
                        dialog.dismiss();
                    }
                }).cancelable(false).dividerColor(context.getResources().getColor(R.color.commBackColor));
        final MaterialDialog dialog = builder.build();
        dialog.show();
    }

    public void setTextColorForMenuItem(MenuItem menuItem, @ColorRes int color) {
        SpannableString spanString = new SpannableString(menuItem.getTitle().toString());
        spanString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), 0, spanString.length(), 0);
        menuItem.setTitle(spanString);
    }

}

