package com.avis.avistechbd.dhamakaavis.Activities;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.Toast;
import com.avis.avistechbd.dhamakaavis.R;
import com.avis.avistechbd.dhamakaavis.network_retrieve_common.CommonListData;
import com.avis.avistechbd.dhamakaavis.network_retrieve_common.SearchViewModel;
import com.avis.avistechbd.dhamakaavis.view_side.ContentList;

import java.util.Objects;

public class SearchContent extends AppCompatActivity {

    private EditText searchContent;
    private static final int COUNT_CHAR = 2;
    private Context context;
    private Activity activity;

    private RecyclerView recyclerView;
    //var
    private final String TAG = "Search Content";

    //Network call
    SearchViewModel searchViewModel;
    Observer<CommonListData> trackSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_content);
        initUIresponse();
        searchViewModel = ViewModelProviders.of((FragmentActivity) activity).get(SearchViewModel.class);
        trackSearch = new Observer<CommonListData>(){
            @Override
            public void onChanged(@Nullable CommonListData commonListData) {
                initRecyclerView(commonListData);
            }
        };
        searchContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(start % COUNT_CHAR == 0){
                    searchViewModel.getCommontListDataConfig(String.valueOf(s)).observe(SearchContent.this,trackSearch);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initUIresponse() {
        context = SearchContent.this;
        activity = SearchContent.this;
        searchContent = findViewById(R.id.search_content);
        recyclerView = findViewById(R.id.searchRecycle);
    }

    public void searchBackPress(View view) {
        finish();
    }

    private void initRecyclerView(@Nullable CommonListData commonListData){
        try{
            ContentList adapter = new ContentList(commonListData,context);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            try{
                LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
                recyclerView.setLayoutAnimation(animation);
                Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                recyclerView.scheduleLayoutAnimation();
            }catch (NullPointerException e){
                Log.d("SearchContent",""+e.getMessage());
            }
        }catch (Exception e){
            showMsg("recyclerView Error "+e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void showMsg(String msg){
        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
    }
}
