package com.avis.avistechbd.dhamakaavis.view_side;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.avis.avistechbd.dhamakaavis.R;

public class mDialogClass extends Dialog implements View.OnClickListener {
    //setStatusFire
    private Menu getNavMenu;
    private Context context;
    public Activity activity;
    public Button yes, no;
    TextView txtDia;
    int dialog_title;
    int flagNo;

    public mDialogClass(Activity activity, int dialog_title, int flagNo, Context context) {
        super(activity);
        this.activity = activity;
        this.dialog_title = dialog_title;
        this.flagNo = flagNo;
        this.context = context;
    }

    public mDialogClass(Activity activity, int dialog_title, int flagNo, Context context, Menu getNavMenu) {
        super(activity);
        this.activity = activity;
        this.dialog_title = dialog_title;
        this.flagNo = flagNo;
        this.context = context;
        this.getNavMenu = getNavMenu;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        yes = (Button) findViewById(R.id.button2);
        no = (Button) findViewById(R.id.button);
        txtDia = (TextView) findViewById(R.id.textView7);
        txtDia.setText(dialog_title);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button2:
                if(flagNo == 1){
                    dismiss();
                    activity.finish();
                }else if(flagNo == 10){
                    showUnsubsDialog(getNavMenu);
                }
                break;
            case R.id.button:
                dismiss();
                break;
            default:
                break;
        }
    }

    public void setTextColorForMenuItem(MenuItem menuItem, @ColorRes int color) {
        SpannableString spanString = new SpannableString(menuItem.getTitle().toString());
        spanString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), 0, spanString.length(), 0);
        menuItem.setTitle(spanString);
    }

    public void showUnsubsDialog(final Menu getNavMenu) {
        getNavMenu.findItem(R.id.nav_subscribe).setTitle("SUBSCRIBE");
        setTextColorForMenuItem(getNavMenu.findItem(R.id.nav_subscribe), R.color.colorPrimary);
        dismiss();
    }
}
