package com.avis.avistechbd.dhamakaavis.network_retrieve_common;

public class PhoneInfoData {
    String phoneNo;

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
}
