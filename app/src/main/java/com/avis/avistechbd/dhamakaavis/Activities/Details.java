package com.avis.avistechbd.dhamakaavis.Activities;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import com.avis.avistechbd.dhamakaavis.R;
import com.avis.avistechbd.dhamakaavis.view_side.DialogBoxOpenUp;
import butterknife.ButterKnife;
public class Details extends AppCompatActivity {

    private static final String TEST_URL = "http://103.17.180.110/cmsdhamaka/FullVideo/D1/Album%20Akash%20Jure%20Brishty%20Singer%20Jharna%20Rahman_Bisshash%20Koro%20Tomake%20ami%20ekhon_4%20min%206%20sec.mp4";
    //http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4
    private DialogBoxOpenUp dopenUp;
    private Handler handler;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);
        context = Details.this;
        dopenUp = new DialogBoxOpenUp(context);
        handler = new Handler(Looper.getMainLooper());
    }
}
