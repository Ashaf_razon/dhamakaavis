package com.avis.avistechbd.dhamakaavis.network_retrieve_common;

import java.util.ArrayList;

public class CommonListData {
    public ArrayList<String> contImageURL = new ArrayList<>();
    public ArrayList<String> contAlbum = new ArrayList<>();
    public ArrayList<String> contTitle = new ArrayList<>();
    public ArrayList<String> contInfo = new ArrayList<>();
    public ArrayList<String> contDuration = new ArrayList<>();
    public ArrayList<String> contLength = new ArrayList<>();
    public ArrayList<String> contVideoURL = new ArrayList<>();
    public ArrayList<String> contVideoID = new ArrayList<>();

    public ArrayList<String> getContLength() {
        return contLength;
    }

    public void setContLenth(ArrayList<String> contLength) {
        this.contLength = contLength;
    }

    public ArrayList<String> getContVideoID() {
        return contVideoID;
    }

    public void setContVideoID(ArrayList<String> contVideoID) {
        this.contVideoID = contVideoID;
    }

    public ArrayList<String> getContImageURL() {
        return contImageURL;
    }

    public void setContImageURL(ArrayList<String> contImageURL) {
        this.contImageURL = contImageURL;
    }

    public ArrayList<String> getContAlbum() {
        return contAlbum;
    }

    public void setContAlbum(ArrayList<String> contAlbum) {
        this.contAlbum = contAlbum;
    }

    public ArrayList<String> getContTitle() {
        return contTitle;
    }

    public void setContTitle(ArrayList<String> contTitle) {
        this.contTitle = contTitle;
    }

    public ArrayList<String> getContInfo() {
        return contInfo;
    }

    public void setContInfo(ArrayList<String> contInfo) {
        this.contInfo = contInfo;
    }

    public ArrayList<String> getContDuration() {
        return contDuration;
    }

    public void setContDuration(ArrayList<String> contDuration) {
        this.contDuration = contDuration;
    }

    public ArrayList<String> getContVideoURL() {
        return contVideoURL;
    }

    public void setContVideoURL(ArrayList<String> contVideoURL) {
        this.contVideoURL = contVideoURL;
    }
}
