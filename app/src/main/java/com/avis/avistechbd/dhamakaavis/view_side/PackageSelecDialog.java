package com.avis.avistechbd.dhamakaavis.view_side;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.avis.avistechbd.dhamakaavis.R;

public class PackageSelecDialog extends Dialog implements View.OnClickListener {
    //setStatusFire
    private Menu getNavMenu;
    private Context context;
    public Activity activity;
    public RadioGroup radioPack;
    public RadioButton getSelectedBt;
    public Button yes, no;
    int dialog_title;
    int flagNo;

    public PackageSelecDialog(Activity activity, int flagNo, Context context, Menu getNavMenu) {
        super(activity);
        this.activity = activity;
        this.flagNo = flagNo;
        this.context = context;
        this.getNavMenu = getNavMenu;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.package_select_dialog);
        yes = (Button) findViewById(R.id.button4);
        no = (Button) findViewById(R.id.button3);
        radioPack = findViewById(R.id.radioGroup);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button4:
                getSelectedPack();
                break;
            case R.id.button3:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }

    private void getSelectedPack() {
        int radioPackId = radioPack.getCheckedRadioButtonId();
        getSelectedBt = findViewById(radioPackId);
        showMsgPack(getSelectedBt.getText().toString());
        showSubsDialog(getNavMenu);
    }

    private void showMsgPack(String msg) {
        Toast.makeText(context,msg+", Selected",Toast.LENGTH_LONG).show();
    }

    public void setTextColorForMenuItem(MenuItem menuItem, @ColorRes int color) {
        SpannableString spanString = new SpannableString(menuItem.getTitle().toString());
        spanString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, color)), 0, spanString.length(), 0);
        menuItem.setTitle(spanString);
    }

    public void showSubsDialog(final Menu getNavMenu) {
        getNavMenu.findItem(R.id.nav_subscribe).setTitle("UNSUBSCRIBE");
        setTextColorForMenuItem(getNavMenu.findItem(R.id.nav_subscribe), R.color.yeloowMy);
    }

}
