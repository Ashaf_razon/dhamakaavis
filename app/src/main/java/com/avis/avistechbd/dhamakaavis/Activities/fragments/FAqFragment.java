package com.avis.avistechbd.dhamakaavis.Activities.fragments;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.avis.avistechbd.dhamakaavis.view_side.FaqListItemView;
import com.avis.avistechbd.dhamakaavis.R;
import com.avis.avistechbd.dhamakaavis.back_view.RecyclerTouchResponse;
import com.avis.avistechbd.dhamakaavis.view_side.QuestionAnsDialog;

import java.util.ArrayList;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FAqFragment extends Fragment {
    private RecyclerView recyclerView;
    //var
    private final String TAG = "Faq_Main";
    private ArrayList<Integer> quesTitle = new ArrayList<Integer>();
    private ArrayList<Integer> quesAns = new ArrayList<>();
    private ArrayList<Integer> quesReply = new ArrayList<>();
    //application var
    private Unbinder unbinder;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_faq, container, false);
        context = getActivity();
        try{
            TextView tv_toolbar = ((FragmentActivity) context).findViewById(R.id.toolbar_title);
            tv_toolbar.setText("FAQ");
        }catch (Exception e){
            Toast.makeText(context,""+e.getMessage(),Toast.LENGTH_LONG).show();
        }
        unbinder = ButterKnife.bind(this, view);
        initValuesRecycler();
        initRecyclerView(view);
        return view;
    }
    private void initValuesRecycler(){
        quesTitle.add(R.string.Q1);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply1);

        quesTitle.add(R.string.Q2);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply2);

        quesTitle.add(R.string.Q3);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply3);

        quesTitle.add(R.string.Q4);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply4);

        quesTitle.add(R.string.Q5);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply5);

        quesTitle.add(R.string.Q6);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply6);

        quesTitle.add(R.string.Q7);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply7);

        quesTitle.add(R.string.Q8);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply8);

        quesTitle.add(R.string.Q9);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply9);

        quesTitle.add(R.string.Q10);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply10);

        quesTitle.add(R.string.Q11);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply11);

        quesTitle.add(R.string.Q12);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply12);

        quesTitle.add(R.string.Q13);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply13);

        quesTitle.add(R.string.Q14);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply14);

        quesTitle.add(R.string.Q15);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply15);
    }
    private void initRecyclerView(View view){
        recyclerView = view.findViewById(R.id.recycler_view);
        FaqListItemView adapter = new FaqListItemView(quesTitle,quesAns,quesReply,context);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addOnItemTouchListener(new RecyclerTouchResponse(context, recyclerView, new RecyclerTouchResponse.ClickResponse() {
            @Override
            public void onSingleTap(View view, int position) {
                //showStatus("item ans: "+getResources().getString(quesReply.get(position)));
                customDialogCall(getResources().getString(quesReply.get(position)));
            }
            @Override
            public void onLongTap(View view, int position) {
            }
        }));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void showStatus(String msg){
        Toast.makeText(context,"FAQ: "+msg,Toast.LENGTH_LONG).show();
    }

    private void customDialogCall(String setTitle) {
        QuestionAnsDialog myDialog = new QuestionAnsDialog((Activity) context, setTitle, 1,context);
        try {
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            myDialog.setCancelable(false);
            myDialog.show();
        } catch (Exception e) {
            showStatus("Error Dialog: "+e.getMessage());
        }
    }
}
