package com.avis.avistechbd.dhamakaavis.Activities.fragments;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.dhamakaavis.R;
import com.avis.avistechbd.dhamakaavis.adapters.RecyclerViewDataAdapter;
import com.avis.avistechbd.dhamakaavis.back_model.SectionDataModel;
import com.avis.avistechbd.dhamakaavis.back_model.SingleItemModel;
import com.avis.avistechbd.dhamakaavis.network_retrieve_common.MainPageListData;
import com.avis.avistechbd.dhamakaavis.network_retrieve_common.MainpageViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ParentFragment extends Fragment {
    private static final int CATEGORY = 4; //last exclusive
    private Unbinder unbinder;
    private Context context;
    private RecyclerView my_recycler_view;
    private RecyclerViewDataAdapter adapter;
    private ArrayList<SectionDataModel> allSampleData;
    private String categoryName [] = {"Music video","Audio zone","Drama zone","Movie clip", "Exclusive"};

    //List Of Contents
    MainpageViewModel mainpageViewModel;
    Observer<MainPageListData> trackMainPageDataList;
    private CarouselView carouselView;

    private ArrayList<SingleItemModel> carouselItemList = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_parent, container, false);
        context = getActivity();
        carouselView = (CarouselView) view.findViewById(R.id.carouselView);
        try{
            TextView tv_toolbar = ((FragmentActivity) context).findViewById(R.id.toolbar_title);
            tv_toolbar.setText("Masti 24");
        }catch (Exception e){
            Toast.makeText(context,""+e.getMessage(),Toast.LENGTH_LONG).show();
        }
        unbinder = ButterKnife.bind(this, view);
        //Multiple Content set
        mainpageViewModel = ViewModelProviders.of(getActivity()).get(MainpageViewModel.class);
        trackMainPageDataList = new Observer<MainPageListData>() {
            @Override
            public void onChanged(@Nullable MainPageListData mainPageListData) {
                viewInitializer(view, mainPageListData); //init main recycler view
            }
        };
        mainpageViewModel.getMainPageListDataConfig().observe(getActivity(),trackMainPageDataList);
        return view;
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            try{
                //imageView.setImageURI(Uri.parse(carouselItemList.get(position)));
                Glide.with(context)
                        .load(carouselItemList.get(position).getUrl())
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
                Log.d("Carousel_Log",""+carouselItemList.get(position));
            }catch (Exception e){
                Log.d("Carousel_Fail",e.getMessage());
            }
        }
    };

    private void viewInitializer(View view, @Nullable MainPageListData mainPageListData) {
        allSampleData = new ArrayList<>();
        createData(mainPageListData);
        try{
            carouselView.setImageListener(imageListener);
            carouselView.setPageCount(carouselItemList.size());
            carouselView.setImageClickListener(new ImageClickListener() {
                @Override
                public void onClick(int position) {
                    Toast.makeText(context, "Clicked item: "+ position, Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.d("Carousel_Fail_size",e.getMessage());
        }
        my_recycler_view = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        my_recycler_view.setHasFixedSize(true);
        adapter = new RecyclerViewDataAdapter(context, allSampleData,mainPageListData, getActivity(), carouselView);
        my_recycler_view.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        runAnimation(my_recycler_view);
    }
    public void createData(@Nullable MainPageListData mainPageListData) {
        for (int i = 0; i < CATEGORY; i++) { //1 for pager
            SectionDataModel dm = new SectionDataModel();
            try{
                dm.setHeaderTitle(categoryName[i]); //all category name set
            }catch (Exception e){
                Log.d("CategoryPrint",e.getMessage());
            }
            ArrayList<SingleItemModel> singleItem = new ArrayList<>(); //all itemLiset set
            for (int j = 0; j < mainPageListData.getContImageURL().get(i).size(); j++) {
                singleItem.add(new SingleItemModel
                        (mainPageListData.getContTitle().get(i).get(j),
                                mainPageListData.getContImageURL().get(i).get(j),
                                mainPageListData.getContVideoURL().get(i).get(j),
                                mainPageListData.getContVideoID().get(i).get(j),
                                mainPageListData.getContDuration().get(i).get(j),
                                mainPageListData.getContLen().get(i).get(j)));
                if(i == (CATEGORY - 1)){
                    carouselItemList.add(new SingleItemModel
                            (mainPageListData.getContTitle().get(CATEGORY).get(j),
                                    mainPageListData.getContImageURL().get(CATEGORY).get(j),
                                    mainPageListData.getContVideoURL().get(CATEGORY).get(j),
                                    mainPageListData.getContVideoID().get(CATEGORY).get(j),
                                    mainPageListData.getContDuration().get(CATEGORY).get(j),
                                    mainPageListData.getContLen().get(CATEGORY).get(j)));
                }
            }
            //All view Data set in setter
            dm.setAllItemsInSection(singleItem);
            //All view Data add in arrayList
            allSampleData.add(dm);
        }
    }

    private void runAnimation(RecyclerView my_recycler_view) {
        LayoutAnimationController rController =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        my_recycler_view.setAdapter(adapter);
        //animation
        my_recycler_view.setLayoutAnimation(rController);
        my_recycler_view.getAdapter().notifyDataSetChanged();
        my_recycler_view.scheduleLayoutAnimation();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
