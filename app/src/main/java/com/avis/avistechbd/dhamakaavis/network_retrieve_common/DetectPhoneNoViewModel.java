package com.avis.avistechbd.dhamakaavis.network_retrieve_common;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONObject;

public class DetectPhoneNoViewModel extends AndroidViewModel {

    private static final int PARAM1 = 0;
    private static final int PARAM2 = 255;
    private static final int PARAM3 = 0;
    private static final int PARAM4 = 0;
    Context context;
    private static final String PARENT_ADD = "http://202.4.114.69/API/example/?reqCode";

    private final String commonURL = PARENT_ADD+"="+PARAM1+"&viewType="+PARAM2+"&incrValue="+PARAM3+"&videoId="+PARAM4+"";
    private String usersPhone;

    public PhoneInfoData phoneInfoData = new PhoneInfoData();
    public MutableLiveData<PhoneInfoData> phoneDataConfig;
    public MutableLiveData<PhoneInfoData> getPhoneDataConfig(Context context){
        Log.d("GET_Phone","Config. "+PARAM1);
        this.context = context;
        if(phoneDataConfig == null){
            phoneDataConfig = new MutableLiveData<>();
            Log.d("GET_PHONE","Config. Call");
            getPhoneDataInfo();
        }
        return phoneDataConfig;
    }

    private void getPhoneDataInfo() {
        StringRequest request = new StringRequest(Request.Method.POST, commonURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response view-model", "Detect-Page: " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    int count  = 0;
                    while(count < jsonArray.length()){
                        JSONObject jsonObject = jsonArray.getJSONObject(count);
                        usersPhone = jsonObject.getString("MSISDN");
                        count++;
                    }
                    phoneInfoData.setPhoneNo(usersPhone);
                    phoneDataConfig.setValue(phoneInfoData);
                    Log.d("response userData", "Success "+usersPhone);
                } catch (Exception e) {
                    Log.d("response view-model", "Exception: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response view-model", "Error Listener: " +error.getMessage());
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        Log.d("response view-model", "out-context: " + getApplication());
        requestQueue.add(request);
    }

    public DetectPhoneNoViewModel(@NonNull Application application) {
        super(application);
    }

    public boolean isNetworkAvailable() {
        boolean flagNetwork = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            flagNetwork = true;
//            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                // connected to wifi
//                Toast.makeText(context,"Wifi is used, Please use Mobile data",Toast.LENGTH_LONG).show();
//            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                // connected to mobile data
//                flagNetwork = true;
//            }
        } else {
            Toast.makeText(context,"Please use Mobile data",Toast.LENGTH_LONG).show();
        }
        return flagNetwork;
    }
}
