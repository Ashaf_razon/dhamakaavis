package com.avis.avistechbd.dhamakaavis.view_side;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.avis.avistechbd.dhamakaavis.R;
import java.util.ArrayList;

public class FaqListItemView extends RecyclerView.Adapter<FaqListItemView.ViewHolder>{
    //class var
    private static final String TAG = "recycler View";
    private ArrayList<Integer> quesTitle;
    private ArrayList<Integer> quesAns;
    private ArrayList<Integer> quesReply;
    private Context context;

    public FaqListItemView(ArrayList<Integer> quesTitle, ArrayList<Integer> quesAns, ArrayList<Integer> quesReply, Context context) {
        this.quesTitle = quesTitle;
        this.quesAns = quesAns;
        this.quesReply = quesReply;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_faq,viewGroup,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Log.d(TAG,"Bind View Holder Called");
        viewHolder.ques.setText(quesTitle.get(i));
    }

    @Override
    public int getItemCount() {
        return quesTitle.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView ques,ans,reply;
        RelativeLayout parentLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ques = itemView.findViewById(R.id.faqQuesid);
            parentLayout = itemView.findViewById(R.id.relative_Layout_item_list);

        }
    }
}
