package com.avis.avistechbd.dhamakaavis.network_retrieve_common;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainpageViewModel extends AndroidViewModel {
    private static final int CATEGORY = 5;
    private static final int PARAM1 = 0;
    private static final int PARAM2 = 200;
    private static final int PARAM3 = 0;
    private static final int PARAM4 = 0;
    private String contTitleSet = "";
    private int contMaxCharLen = 11;
    private static final String PARENT_ADD = "http://202.4.114.69/API/example/?reqCode";

    private String mainURL;
    private static final String IMAGE_SRC = "http://103.17.180.110/cmsdhamaka/ImageVideo/D1/";
    private static final String VIDEO_SRC = "http://103.17.180.110/cmsdhamaka/FullVideo/D1/";
    private String catId_1 = "3F18B803D4E44812BE7E88DD365F5780"; //video
    private String catId_2 = "3F71158EE33F412ABE134AC04B8FA7F4"; //Audio
    private String catId_3 = "431CD0BA81AA4492B3818CDA55819B79"; //Drama
    private String catId_4 = "C61B79DDC7A9444DA17256B5427024B7"; //Movie
    private String catId_exclusive = "A5FC9CA24C3C4300B92742311CDABF6F";
    //2D matrix
    ArrayList<ArrayList<String>> contImageURL = new ArrayList<>();
    ArrayList<ArrayList<String>> contAlbum = new ArrayList<>();
    ArrayList<ArrayList<String>> contTitle = new ArrayList<>();
    ArrayList<ArrayList<String>> contInfo = new ArrayList<>();
    ArrayList<ArrayList<String>> contDuration = new ArrayList<>(); //viewNo
    ArrayList<ArrayList<String>> contLength= new ArrayList<>();
    ArrayList<ArrayList<String>> contVideoURL = new ArrayList<>();
    ArrayList<ArrayList<String>> contVideoID = new ArrayList<>();
    //ROW list

    MainPageListData mainPageListData = new MainPageListData();
    public MutableLiveData<MainPageListData> mainPageListDataConfig;
    public MutableLiveData<MainPageListData> getMainPageListDataConfig(){
        if(mainPageListDataConfig == null){
            mainPageListDataConfig = new MutableLiveData<>();
            getCommonDataInfo(); //call 4 times for FOUR layers
        }
        return mainPageListDataConfig;
    }

    private void getCommonDataInfo() {
        //space for category
        for(int i = 0; i < CATEGORY; i++){
            contImageURL.add(new ArrayList<String>());
            contAlbum.add(new ArrayList<String>());
            contTitle.add(new ArrayList<String>());
            contInfo.add(new ArrayList<String>());
            contVideoID.add(new ArrayList<String>());
            contDuration.add(new ArrayList<String>());
            contLength.add(new ArrayList<String>());
            contVideoURL.add(new ArrayList<String>());
        }

        mainURL = PARENT_ADD+"="+PARAM1+"&viewType="+PARAM2+"&incrValue="+PARAM3+"&videoId="+PARAM4+"";
        StringRequest request = new StringRequest(Request.Method.GET, mainURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               Log.d("response view-model", "-> Main-Page: " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    int count  = 0;
                    while(count < jsonArray.length()){
                        JSONObject jsonObject = jsonArray.getJSONObject(count);
                        String flagCat = jsonObject.getString("catID");

                        String imageUrlRow = IMAGE_SRC+jsonObject.getString("ImageUrl").replace(" ","%20");
                        String albumRow = jsonObject.getString("Album");
                        String titleRow = jsonObject.getString("Title");
                        if(titleRow.length() >= contMaxCharLen){
                            titleRow += "..";
                        }
                        String infoRow = jsonObject.getString("Info");
                        String videoIdRow = jsonObject.getString("videoId");
                        String durationRow = jsonObject.getString("viewNo");
                        String contLen = jsonObject.getString("Duration");
                        String videoUrlRow = VIDEO_SRC+jsonObject.getString("VideoUrl").replace(" ","%20");

                        if(flagCat.compareToIgnoreCase(catId_1) == 0){ //MUSIC_VIDEO
                            contImageURL.get(0).add(imageUrlRow);
                            contAlbum.get(0).add(albumRow);
                            contTitle.get(0).add(titleRow);
                            contInfo.get(0).add(infoRow);
                            contVideoID.get(0).add(videoIdRow);
                            contDuration.get(0).add(durationRow);
                            contLength.get(0).add(contLen);
                            contVideoURL.get(0).add(videoUrlRow);
                        }else if(flagCat.compareToIgnoreCase(catId_2) == 0){ //AUDIO_ZONE
                            contImageURL.get(1).add(imageUrlRow);
                            contAlbum.get(1).add(albumRow);
                            contTitle.get(1).add(titleRow);
                            contInfo.get(1).add(infoRow);
                            contVideoID.get(1).add(videoIdRow);
                            contDuration.get(1).add(durationRow);
                            contLength.get(1).add(contLen);
                            contVideoURL.get(1).add(videoUrlRow);
                        }else if(flagCat.compareToIgnoreCase(catId_3) == 0){ //DRAMA_ZONE
                            contImageURL.get(2).add(imageUrlRow);
                            contAlbum.get(2).add(albumRow);
                            contTitle.get(2).add(titleRow);
                            contInfo.get(2).add(infoRow);
                            contVideoID.get(2).add(videoIdRow);
                            contDuration.get(2).add(durationRow);
                            contLength.get(2).add(contLen);
                            contVideoURL.get(2).add(videoUrlRow);
                        }else if(flagCat.compareToIgnoreCase(catId_4) == 0){ //CINEMA_ZONE
                            contImageURL.get(3).add(imageUrlRow);
                            contAlbum.get(3).add(albumRow);
                            contTitle.get(3).add(titleRow);
                            contInfo.get(3).add(infoRow);
                            contVideoID.get(3).add(videoIdRow);
                            contDuration.get(3).add(durationRow);
                            contLength.get(3).add(contLen);
                            contVideoURL.get(3).add(videoUrlRow);
                        }else{ //Exclusive
                            contImageURL.get(4).add(imageUrlRow);
                            contAlbum.get(4).add(albumRow);
                            contTitle.get(4).add(titleRow);
                            contInfo.get(4).add(infoRow);
                            contVideoID.get(4).add(videoIdRow);
                            contDuration.get(4).add(durationRow);
                            contLength.get(4).add(contLen);
                            contVideoURL.get(4).add(videoUrlRow);
                        }

                        count++;
                    }
                    try{
                        mainPageListData.setContImageURL(contImageURL);
                        mainPageListData.setContAlbum(contAlbum);
                        mainPageListData.setContTitle(contTitle);
                        mainPageListData.setContInfo(contInfo);
                        mainPageListData.setContVideoID(contVideoID);
                        mainPageListData.setContDuration(contDuration);
                        mainPageListData.setContLen(contLength);
                        mainPageListData.setContVideoURL(contVideoURL);
                        for(int i = 0; i< contImageURL.size();i++){
                            Log.d("main view-model", contImageURL.size()+" -> Main-Page: " + contImageURL.get(i));
                        }
                        mainPageListDataConfig.setValue(mainPageListData);
                    }catch (Exception e){
                        Log.d("main view-model", "-> Main-Page erMsg"+e.getMessage());
                    }


                } catch (Exception e) {
                    Log.d("response view-model", "Exception: " + e.getMessage());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response view-model", "Error Listener: " +error.getMessage());
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        Log.d("response view-model", "out-context: " + getApplication());
        requestQueue.add(request);
    }

    public MainpageViewModel(@NonNull Application application) {
        super(application);
    }
}
