package com.avis.avistechbd.dhamakaavis.adapters;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.dhamakaavis.Activities.AudioPlay;
import com.avis.avistechbd.dhamakaavis.Activities.VideoPreview;
import com.avis.avistechbd.dhamakaavis.R;
import com.avis.avistechbd.dhamakaavis.back_model.SingleItemModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {

    private ArrayList<SingleItemModel> itemsList;
    private Context mContext;
    private SingleItemModel singleItem;
    CarouselView carouselView;
    public SectionListDataAdapter(Context context, ArrayList<SingleItemModel> itemsList, CarouselView carouselView) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.carouselView = carouselView;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, final int i) {
        singleItem = itemsList.get(i);
        holder.tvTitle.setText(singleItem.getName());
        holder.itemView.setText(singleItem.getNoOfView());

        Glide.with(this.mContext)
                .load(singleItem.getUrl())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.itemImage);
        holder.mcCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(itemsList.get(i).getVideoUrl().contains(".mp3")){
                    Intent intent = new Intent(mContext, AudioPlay.class);
                    intent.putExtra("audioLen",itemsList.get(i).getContLen());
                    intent.putExtra("audioID",itemsList.get(i).getVideoID());
                    intent.putExtra("audioURL",itemsList.get(i).getVideoUrl());
                    intent.putExtra("audioImageURL",itemsList.get(i).getUrl());
                    (mContext).startActivity(intent);
                }else{
                    //play video
                    Intent intent = new Intent(mContext, VideoPreview.class);
                    intent.putExtra("videoURL",itemsList.get(i).getVideoUrl());
                    (mContext).startActivity(intent);
                }
                //Toast.makeText(v.getContext(), itemsList.get(i).getName()+" "+itemsList.get(i).getVideoUrl(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {
        protected TextView tvTitle, itemView;
        protected ImageView itemImage;
        protected CardView mcCardView;

        //per Content selection response
        public SingleItemRowHolder(View view) {
            super(view);
            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemView = (TextView) view.findViewById(R.id.itemView);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
            this.mcCardView = view.findViewById(R.id.parentFragmentContentId);
        }

    }

}