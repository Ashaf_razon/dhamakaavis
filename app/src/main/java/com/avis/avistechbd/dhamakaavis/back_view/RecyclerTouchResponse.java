package com.avis.avistechbd.dhamakaavis.back_view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class RecyclerTouchResponse implements RecyclerView.OnItemTouchListener {
    private GestureDetector gestureDetector;
    private ClickResponse clickResponse;

    public RecyclerTouchResponse(Context context,final RecyclerView recyclerView, final ClickResponse clickResponse) {
        this.clickResponse = clickResponse;
        gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && clickResponse != null) {
                    clickResponse.onSingleTap(child, recyclerView.getChildLayoutPosition(child));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent e) {
        View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
        if (child != null && clickResponse != null && gestureDetector.onTouchEvent(e)) {
            clickResponse.onSingleTap(child, recyclerView.getChildLayoutPosition(child));
        }
        return false;
    }

    @Override
    public void onTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean b) {

    }

    public interface ClickResponse{
        void onSingleTap(View view,int position);
        void onLongTap(View view, int position);
    }
}
