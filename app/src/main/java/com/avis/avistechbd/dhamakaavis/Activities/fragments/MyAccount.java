package com.avis.avistechbd.dhamakaavis.Activities.fragments;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.avis.avistechbd.dhamakaavis.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyAccount extends Fragment {
    private Unbinder unbinder;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my__account, container, false);
        context = getActivity();
        try{
            TextView tv_toolbar = ((FragmentActivity) context).findViewById(R.id.toolbar_title);
            tv_toolbar.setText("My Account");
        }catch (Exception e){
            Toast.makeText(context,""+e.getMessage(),Toast.LENGTH_LONG).show();
        }
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
