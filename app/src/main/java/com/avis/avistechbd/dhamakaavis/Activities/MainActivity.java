package com.avis.avistechbd.dhamakaavis.Activities;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.avis.avistechbd.dhamakaavis.Activities.fragments.FAqFragment;
import com.avis.avistechbd.dhamakaavis.Activities.fragments.ListOfContent;
import com.avis.avistechbd.dhamakaavis.Activities.fragments.MyAccount;
import com.avis.avistechbd.dhamakaavis.Activities.fragments.ParentFragment;
import com.avis.avistechbd.dhamakaavis.R;
import com.avis.avistechbd.dhamakaavis.view_side.PackageSelecDialog;
import com.avis.avistechbd.dhamakaavis.view_side.mDialogClass;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //Data variables
    boolean usersStatus;
    String usersPhone;
    ImageView searchButt;
    final static int HOME_VIEW = 1;
    final static int MUSIC_VIDEO = 2;
    final static int AUDIO_ZONE = 3;
    final static int DRAMA_ZONE = 4;
    final static int CINEMA_ZONE = 5;
    final static int MY_ACCOUNT = 6;
    final static int FAQ_VIEW = 7;
    int FLAG_set = 7;
    public int resultFlag = 0;

    //View Variables
    Animation myAnim;
    @BindView(R.id.toolbar) Toolbar toolbar;
//    @BindView(R.id.userAccountName)
//    TextView userAccountName;
//    @BindView(R.id.userAccountPhone)
    TextView userAccountPhone;
    private NavigationView navigationView;
    private Menu getNavMenu;
    private String getMenuSubsName = null;
    private String setDialMsg = null;
    private Context context;
    protected String setCardId = null;
    private PackageSelecDialog packDialOpen;
    private mDialogClass myDialogPack;
    private Handler handler;
    private ArrayList<String> itemTitle = new ArrayList();
    private ArrayList<String> itemPhoto = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        setContentView(R.layout.activity_main);
        searchButt = findViewById(R.id.imageView5);
        Bundle bundle = getIntent().getExtras();
        usersPhone = bundle.getString("user_phone");

        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getNavMenu = navigationView.getMenu();
        try{
            MenuItem mItem = getNavMenu.findItem(R.id.accCommunicate);
            //MenuItem mHome = getNavMenu.findItem(R.id.nav_Home);
            SpannableString s = new SpannableString(mItem.getTitle());
            s.setSpan(new TextAppearanceSpan(this, R.style.NavGroupHeadColor), 0, s.length(), 0);
            mItem.setTitle(s);
        }catch (Exception e){}
        disableNavigationViewScrollbars(navigationView); //disable scrollbar
        handler = new Handler(Looper.getMainLooper());
        context = MainActivity.this;
        packDialOpen = new PackageSelecDialog((Activity) context, 10,context, getNavMenu);
        packDialOpen.setTextColorForMenuItem(getNavMenu.findItem(R.id.nav_subscribe), R.color.txtRed);
        //HomeFragmentView
        View hView = navigationView.getHeaderView(0);
        userAccountPhone =hView.findViewById(R.id.userAccountPhone);
        detectUserPhoneStatus();
    }

    private void detectUserPhoneStatus() {
        //DoItBackground doBack = new DoItBackground(context, userAccountPhone);
        try{
            if(isNetworkAvailable()) {
                //if Internet is available
                create_users_content_view(HOME_VIEW);
                usersStatus = true; //Users app authentication
                //doBack.execute();
                userAccountPhone.setText("Phone: "+usersPhone);
            }
            else{
                usersStatus = false;
                userAccountPhone.setText("Please use mobile data / Internet");
            }
        }catch (Exception e){
            Log.d("MainActivity","setDev info : "+e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            customDialogCall(R.string.st_warn_exit);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        try{
            for(int i = 0; i < menu.size(); i++) {
                MenuItem item = menu.getItem(i);
                SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
                spanString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.commonColor)), 0,spanString.length(), 0); //fix the color to white
                item.setTitle(spanString);
            }
        }catch (Exception e){}
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent getSettings = new Intent(MainActivity.this,SettingsActivity.class);

        if (id == R.id.action_settings) {
            getSettings.putExtra("SETTINGS",1);
            startActivity(getSettings);
        }
//        else if(id == R.id.privacyPolicy){
//            getSettings.putExtra("SETTINGS",2);
//            startActivity(getSettings);
//        }else if(id == R.id.whyWe){
//            getSettings.putExtra("SETTINGS",3);
//            startActivity(getSettings);
//        }
        else if(id == R.id.termsCond){
            getSettings.putExtra("SETTINGS",4);
            startActivity(getSettings);
        }else{
            customDialogCall(R.string.st_warn_exit);
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_Home) {
            searchButt.setVisibility(View.VISIBLE);
            create_users_content_view(HOME_VIEW);
        }else if (id == R.id.music_video_items) {
           // callDetailsActivityForContent(1);
            searchButt.setVisibility(View.VISIBLE);
            create_users_content_view(MUSIC_VIDEO);
        } else if (id == R.id.audio_song_items) {
            //callDetailsActivityForContent(2);
            searchButt.setVisibility(View.VISIBLE);
            create_users_content_view(AUDIO_ZONE);
        }else if (id == R.id.drama_videos) {
            searchButt.setVisibility(View.VISIBLE);
            create_users_content_view(DRAMA_ZONE);
        }else if (id == R.id.movie_videos) {
            searchButt.setVisibility(View.VISIBLE);
            create_users_content_view(CINEMA_ZONE);
        }else if (id == R.id.my_acc) {
            searchButt.setVisibility(View.GONE);
            create_users_content_view(MY_ACCOUNT);
        }else if (id == R.id.nav_subscribe) {
            searchButt.setVisibility(View.VISIBLE);
            myDialogPack = new mDialogClass((Activity) context, R.string.st_pack_unsubs, 10,context,getNavMenu);
            getMenuSubsName = item.getTitle().toString();
            if(getMenuSubsName.compareToIgnoreCase("SUBSCRIBE") == 0){
                packDialOpen.setCancelable(false);
                packDialOpen.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                packDialOpen.show();
            }else if(getMenuSubsName.compareToIgnoreCase("UNSUBSCRIBE") == 0){
                myDialogPack.setCancelable(false);
                myDialogPack.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialogPack.show();
            }
        } else if (id == R.id.nav_faq) {
            searchButt.setVisibility(View.GONE);
            create_users_content_view(FAQ_VIEW);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//    private void callDetailsActivityForContent(final int getCardId) {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        try{
//                            Intent intentNext;
//                            if(getCardId == 1){
//                                intentNext = new Intent(context, Details.class);
//                            }else{
//                                intentNext = new Intent(context, VideoPreview.class);
//                            }
//                            startActivity(intentNext);
//                        }catch (Exception e){
//                            e.printStackTrace();
//                        }
//                    }
//                });
//            }
//        }).start();
//    }
    private void callFAQmethod() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            Intent faQ = new Intent(MainActivity.this,FaqActivity.class);
                            startActivity(faQ);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public boolean isNetworkAvailable() {
        boolean flagNetwork = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            flagNetwork = true;
//            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
//                // connected to wifi
//                Toast.makeText(context,"Wifi is used, Please use Mobile data",Toast.LENGTH_LONG).show();
//            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                // connected to mobile data
//                flagNetwork = true;
//            }
        } else {
            Toast.makeText(context,"Please use Mobile data",Toast.LENGTH_LONG).show();
        }
        return flagNetwork;
    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    private void create_users_content_view(int getViewId){
        try{
            switch (getViewId){
                case 1:
                    resultFlag = HOME_VIEW;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ParentFragment()).commit();break;
                case 2:
                    resultFlag = MUSIC_VIDEO;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListOfContent()).commit();break;
                case 3:
                    resultFlag = AUDIO_ZONE;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListOfContent()).commit();break;
                case 4:
                    resultFlag = DRAMA_ZONE;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListOfContent()).commit();break;
                case 5:
                    resultFlag = CINEMA_ZONE;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ListOfContent()).commit();break;
                case 6:
                    resultFlag = MY_ACCOUNT;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MyAccount()).commit();break;
                case 7:
                    resultFlag = FAQ_VIEW;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new FAqFragment()).commit(); break;
                default:
                    resultFlag = HOME_VIEW;
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ParentFragment()).commit(); break;
            }
        }catch (Exception e){
            Log.d("MAIN_ACTIVITY","CREATE_VIEW: "+e.getMessage());
            showUsersMessage("View error, "+e.getMessage());
        }
    }


    //user defined method
    private void showUsersMessage(String msg){
        Toast.makeText(context,msg,Toast.LENGTH_LONG).show();
    }

    private void customDialogCall(int setTitle) {
        myAnim = AnimationUtils.loadAnimation(context, R.anim.dialog_bounch);
        mDialogClass myDialog = new mDialogClass((Activity) context, setTitle, 1,context);
        try {
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            myDialog.setCancelable(false);
            //myDialog.getWindow().getAttributes().windowAnimations = R.anim.dialog_bounch;
            myDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getSetFragmentSelectionFlag(){
        return resultFlag;
    }

    public void onSearchClick(View view) {
        Intent goForSearch = new Intent(context, SearchContent.class);
        startActivity(goForSearch);
    }
}