package com.avis.avistechbd.dhamakaavis.Activities;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import com.avis.avistechbd.dhamakaavis.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class AudioPlay extends AppCompatActivity implements View.OnTouchListener, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener {
    TextView tv;
    ImageView imageAudio;
    SeekBar seekBarProgress;
    Button playAudioBT;
    String audioLen = "", audioURL = "", audioID = "", audioImageURL = "";
    private MediaPlayer mediaPlayer;
    private int playbackPosition=0;
    private final Handler handler = new Handler();
    private int mediaFileLengthInMilliseconds;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_play);
        playAudioBT = findViewById(R.id.button_start);
        seekBarProgress = findViewById(R.id.seekBar);
        seekBarProgress.setMax(99); // It means 100% .0-99
        seekBarProgress.setOnTouchListener(this);
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnCompletionListener(this);
        imageAudio = findViewById(R.id.audioPlayImage);
        Bundle bundle = getIntent().getExtras();
        try{
            audioLen = bundle.getString("audioLen");
            audioID = bundle.getString("audioID");
            audioURL = bundle.getString("audioURL");
            audioImageURL = bundle.getString("audioImageURL");
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            Glide.with(this)
                    .load(audioImageURL)
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageAudio);
        }catch (Exception e){
            e.printStackTrace();
        }
        //tv.setText(audioID+" <> "+audioLen+" <> "+audioURL+" <> "+audioImageURL);
    }

    public void doStart(View view) {
/** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
        try {
            mediaPlayer.setDataSource(audioURL); // setup song from https://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
            mediaPlayer.prepare(); // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
        } catch (Exception e) {
            e.printStackTrace();
        }

        mediaFileLengthInMilliseconds = mediaPlayer.getDuration(); // gets the song length in milliseconds from URL

        if(!mediaPlayer.isPlaying()){
            mediaPlayer.start();
           playAudioBT.setBackground(getDrawable(R.drawable.ic_pause));
        }else {
            mediaPlayer.pause();
            playAudioBT.setBackground(getDrawable(R.drawable.ic_play));
        }
        primarySeekBarProgressUpdater();
    }

    public void doRewind(View view) {
        int currentPosition = this.mediaPlayer.getCurrentPosition();
        int duration = this.mediaPlayer.getDuration();
        // 5 seconds.
        int SUBTRACT_TIME = 5000;
        if(currentPosition - SUBTRACT_TIME > 0 )  {
            this.mediaPlayer.seekTo(currentPosition - SUBTRACT_TIME);
        }
    }

    public void doPause(View view) {
    }

    public void doFastForward(View view) {
        int currentPosition = this.mediaPlayer.getCurrentPosition();
        int duration = this.mediaPlayer.getDuration();
        // 5 seconds.
        int ADD_TIME = 5000;

        if(currentPosition + ADD_TIME < duration)  {
            this.mediaPlayer.seekTo(currentPosition + ADD_TIME);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() == R.id.seekBar){
            /** Seekbar onTouch event handler. Method which seeks MediaPlayer to seekBar primary progress position*/
            if(mediaPlayer.isPlaying()){
                SeekBar sb = (SeekBar)v;
                int playPositionInMillisecconds = (mediaFileLengthInMilliseconds / 100) * sb.getProgress();
                mediaPlayer.seekTo(playPositionInMillisecconds);
            }
        }
        return false;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        seekBarProgress.setSecondaryProgress(percent);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        playAudioBT.setBackground(getDrawable(R.drawable.ic_play));
    }

    /** Method which updates the SeekBar primary progress by current song playing position*/
    private void primarySeekBarProgressUpdater() {
        seekBarProgress.setProgress((int)(((float)mediaPlayer.getCurrentPosition()/mediaFileLengthInMilliseconds)*100)); // This math construction give a percentage of "was playing"/"song length"
        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                   try{
                       primarySeekBarProgressUpdater();
                   }catch (Exception e){
                       e.printStackTrace();
                   }
                }
            };
            handler.postDelayed(notification,1000);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        killMediaPlayer();
    }

    private void killMediaPlayer() {
        if(mediaPlayer!=null) {
            try {
                mediaPlayer.release();
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

//    @Override
//    protected void onPause() {
//        if(mediaPlayer.isPlaying()){
//            mediaPlayer.pause();
//        }
//        super.onPause();
//    }
}
