package com.avis.avistechbd.dhamakaavis.back_model;



public class SingleItemModel {

    private String name;
    private String url;
    private String videoUrl;
    private String videoID;
    private String description;
    private String noOfView;
    private String contLen;

    public SingleItemModel() {
    }

    public SingleItemModel(String name, String url, String videoUrl, String videoID, String noOfView, String contLen) {
        this.name = name;
        this.url = url;
        this.videoUrl = videoUrl;
        this.videoID = videoID;
        this.noOfView = noOfView;
        this.contLen = contLen;
    }

    public String getContLen() {
        return contLen;
    }

    public void setContLen(String contLen) {
        this.contLen = contLen;
    }

    public String getNoOfView() {
        return noOfView;
    }

    public void setNoOfView(String noOfView) {
        this.noOfView = noOfView;
    }

    public String getVideoID() {
        return videoID;
    }

    public void setVideoID(String videoID) {
        this.videoID = videoID;
    }

    public String getUrl() {
        return url;
    }

    public String getVideoUrl() {
        return videoUrl;
    }
    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
