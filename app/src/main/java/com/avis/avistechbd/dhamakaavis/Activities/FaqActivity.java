package com.avis.avistechbd.dhamakaavis.Activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.avis.avistechbd.dhamakaavis.view_side.FaqListItemView;
import com.avis.avistechbd.dhamakaavis.R;
import com.avis.avistechbd.dhamakaavis.back_view.RecyclerTouchResponse;

import java.util.ArrayList;

public class FaqActivity extends AppCompatActivity {

    private final String TAG = "Faq_Main";
    private ArrayList<Integer> quesTitle = new ArrayList<Integer>();
    private ArrayList<Integer> quesAns = new ArrayList<>();
    private ArrayList<Integer> quesReply = new ArrayList<>();

    //App var
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        Log.d(TAG,"Log Print");
        initValuesRecycler();
        setTitle("FAQ");
        context = FaqActivity.this;
        getSupportActionBar().hide();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
        this.finish();
    }

    private void initValuesRecycler(){
        quesTitle.add(R.string.Q1);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply1);

        quesTitle.add(R.string.Q2);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply2);

        quesTitle.add(R.string.Q3);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply3);

        quesTitle.add(R.string.Q4);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply4);

        quesTitle.add(R.string.Q5);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply5);

        quesTitle.add(R.string.Q6);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply6);

        quesTitle.add(R.string.Q7);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply7);

        quesTitle.add(R.string.Q8);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply8);

        quesTitle.add(R.string.Q9);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply9);

        quesTitle.add(R.string.Q10);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply10);

        quesTitle.add(R.string.Q11);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply11);

        quesTitle.add(R.string.Q12);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply12);

        quesTitle.add(R.string.Q13);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply13);

        quesTitle.add(R.string.Q14);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply14);

        quesTitle.add(R.string.Q15);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply15);

        quesTitle.add(R.string.Q16);
        quesAns.add(R.string.Qans1);
        quesReply.add(R.string.Qreply16);

        initRecyclerView();
    }

    private void initRecyclerView(){
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        FaqListItemView adapter = new FaqListItemView(quesTitle,quesAns,quesReply,this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnItemTouchListener(new RecyclerTouchResponse(getApplicationContext(), recyclerView, new RecyclerTouchResponse.ClickResponse() {
            @Override
            public void onSingleTap(View view, int position) {
                showStatus("item pos short: "+position);
            }

            @Override
            public void onLongTap(View view, int position) {
                showStatus("item pos long: "+position);
            }
        }));
    }

    private void showStatus(String msg){
        Toast.makeText(context,"FAQ: "+msg,Toast.LENGTH_LONG).show();
    }
}
