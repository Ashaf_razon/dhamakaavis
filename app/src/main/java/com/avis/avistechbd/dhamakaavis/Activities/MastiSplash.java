package com.avis.avistechbd.dhamakaavis.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.avis.avistechbd.dhamakaavis.network_retrieve_common.DetectPhoneNoViewModel;
import com.avis.avistechbd.dhamakaavis.network_retrieve_common.PhoneInfoData;

public class MastiSplash extends AppCompatActivity {

    private String getUsersPhone="";
    DetectPhoneNoViewModel detectPhoneNoViewModel;
    Observer<PhoneInfoData> trackPhoneData;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getSend Users Phone no
        //Multiple Content set
        context = MastiSplash.this;

        detectPhoneNoViewModel = ViewModelProviders.of(MastiSplash.this).get(DetectPhoneNoViewModel.class);
        trackPhoneData = new Observer<PhoneInfoData>() {
            @Override
            public void onChanged(@Nullable PhoneInfoData phoneInfoData) {
                getUsersPhone = phoneInfoData.getPhoneNo();
                callMainPage(getUsersPhone);
            }
        };
        detectPhoneNoViewModel.getPhoneDataConfig(context).observe(MastiSplash.this,trackPhoneData);
    }

    private void callMainPage(String getUsersPhone) {
        Intent goWapSlider = new Intent(context,MainActivity.class);
        goWapSlider.putExtra("user_phone",""+getUsersPhone);
        try{
            startActivity(goWapSlider);
        }catch (Exception e){
            Log.d("PHONE_EXCEP",""+getUsersPhone+" ERROR: "+e.getMessage());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
