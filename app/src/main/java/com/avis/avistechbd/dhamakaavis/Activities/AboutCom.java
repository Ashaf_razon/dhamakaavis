package com.avis.avistechbd.dhamakaavis.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ScrollView;
import android.widget.TextView;
import com.avis.avistechbd.dhamakaavis.R;

public class AboutCom extends AppCompatActivity {
    ScrollView sc;
    TextView descUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_com);
        sc = (ScrollView) findViewById(R.id.aboutScrollView);
        descUs = findViewById(R.id.descText_show);
        setTitle("We are");
    }

    @Override
    protected void onPause() {
        super.onPause();
        //finish();
    }
    @Override
    protected void onStop() {
        super.onStop();
        this.finish();
    }
}
