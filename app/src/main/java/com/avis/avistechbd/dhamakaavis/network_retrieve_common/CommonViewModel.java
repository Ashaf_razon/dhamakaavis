package com.avis.avistechbd.dhamakaavis.network_retrieve_common;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class CommonViewModel extends AndroidViewModel {
    private int PARAM1 = 1;
    private static final int PARAM2 = 100;
    private static final int PARAM3 = 0;
    private static final int PARAM4 = 0;
    private static final String PARENT_ADD = "http://202.4.114.69/API/example/?reqCode";
    private static final String IMAGE_SRC = "http://103.17.180.110/cmsdhamaka/ImageVideo/D1/";
    private static final String VIDEO_SRC = "http://103.17.180.110/cmsdhamaka/FullVideo/D1/";

    private String commonURL;
    ArrayList<String> contImageURL = new ArrayList<>();
    ArrayList<String> contAlbum = new ArrayList<>();
    ArrayList<String> contTitle = new ArrayList<>();
    ArrayList<String> contInfo = new ArrayList<>();
    ArrayList<String> contDuration = new ArrayList<>(); //viewNo
    ArrayList<String> contLength = new ArrayList<>();
    ArrayList<String> contVideoURL = new ArrayList<>();
    ArrayList<String> contVideoID = new ArrayList<>();

    public CommonListData commontListData = new CommonListData();
    public MutableLiveData<CommonListData> commontListDataConfig;
    public MutableLiveData<CommonListData> getCommontListDataConfig(int FLAG_PARAM1){
        PARAM1 = FLAG_PARAM1;
        commonURL = PARENT_ADD+"="+PARAM1+"&viewType="+PARAM2+"&incrValue="+PARAM3+"&videoId="+PARAM4+"";
        Log.d("GET_FRAGMENT","Config. "+PARAM1);
        initSizeZero();
        if(commontListDataConfig == null){
            commontListDataConfig = new MutableLiveData<>();
            Log.d("GET_FRAGMENT","Config. Call");
            getCommonDataInfo();
        }else{
            commontListDataConfig = new MutableLiveData<>();
            Log.d("GET_FRAGMENT","Config. Call");
            getCommonDataInfo();
        }

        return commontListDataConfig;
    }

    private void getCommonDataInfo() {
        StringRequest request = new StringRequest(Request.Method.POST, commonURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response view-model", "Common-Page: " + response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    int count  = 0;
                    while(count < jsonArray.length()){
                        JSONObject jsonObject = jsonArray.getJSONObject(count);
                        contImageURL.add(IMAGE_SRC+jsonObject.getString("ImageUrl").replace(" ","%20"));
                        contAlbum.add(jsonObject.getString("Album"));
                        contTitle.add(jsonObject.getString("Title"));
                        contInfo.add(jsonObject.getString("Info"));
                        contDuration.add(jsonObject.getString("viewNo"));
                        contLength.add(jsonObject.getString("Duration"));
                        contVideoID.add(jsonObject.getString("videoId"));
                        contVideoURL.add(VIDEO_SRC+jsonObject.getString("VideoUrl").replace(" ","%20"));
                        count++;
                    }
                    commontListData.setContImageURL(contImageURL);
                    commontListData.setContAlbum(contAlbum);
                    commontListData.setContTitle(contTitle);
                    commontListData.setContInfo(contInfo);
                    commontListData.setContDuration(contDuration);
                    commontListData.setContLenth(contLength);
                    commontListData.setContVideoID(contVideoID);
                    commontListData.setContVideoURL(contVideoURL);

                    commontListDataConfig.setValue(commontListData);

                    Log.d("response view-model", "Success");
                } catch (Exception e) {
                    Log.d("response view-model", "Exception: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("response view-model", "Error Listener: " +error.getMessage());
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        Log.d("response view-model", "out-context: " + getApplication());
        requestQueue.add(request);
    }

    public CommonViewModel(@NonNull Application application) {
        super(application);
    }

    private void initSizeZero(){
        contImageURL.clear();
        contAlbum.clear();
        contTitle.clear();
        contInfo.clear();
        contDuration.clear(); //viewNo
        contVideoURL.clear();
        contVideoID.clear();
    }
}
