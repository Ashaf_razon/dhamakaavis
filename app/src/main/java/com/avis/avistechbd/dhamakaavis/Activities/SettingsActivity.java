package com.avis.avistechbd.dhamakaavis.Activities;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.avis.avistechbd.dhamakaavis.R;

public class SettingsActivity extends AppCompatActivity {
    private int getValue = 1;
    TextView tvSettings1,tvSettings2,tvSettings3,tvSettings4;
    ImageView imageSettings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        imageSettings = findViewById(R.id.settinsgimage);
        tvSettings1 = findViewById(R.id.settingsTxt1);
        tvSettings2 = findViewById(R.id.settingsTxt2);
        tvSettings3 = findViewById(R.id.settingsTxt3);
        tvSettings4 = findViewById(R.id.settingsTxt4);

        try{
            getValue = getIntent().getIntExtra("SETTINGS",1);
        }catch (Exception e){
            e.printStackTrace();
        }
        switch (getValue){
            case 1: setAbout(); break;
            case 2: setPrivacyPolicy(); break;
            case 3: setWhyWe(); break;
            case 4: setTermsAndCondition(); break;
            default: setAbout();
        }
    }

    private void setAbout() {
        tvSettings1.setText(R.string.avis_about);
        tvSettings2.setText(R.string.avis_contact);
        tvSettings3.setVisibility(View.GONE);
        tvSettings4.setVisibility(View.GONE);
    }

    private void setPrivacyPolicy() {
        tvSettings1.setText("");
        tvSettings2.setText("");
        tvSettings3.setText("");
        tvSettings4.setText("");
    }

    private void setWhyWe() {
        tvSettings1.setText("");
        tvSettings2.setText("");
        tvSettings3.setText("");
        tvSettings4.setText("");
    }

    private void setTermsAndCondition() {
        tvSettings1.setText(R.string.masti_intro);
        tvSettings2.setText(R.string.masti_intro_2);
        tvSettings3.setText(R.string.masti_act_deact);
        tvSettings4.setText(R.string.masti_portal_link);
    }


}
